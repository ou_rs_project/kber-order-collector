<?php
use PhpOffice\PhpSpreadsheet\Cell;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

/**
 * Created by PhpStorm.
 * User: Leo
 * Date: 5/3/2017
 * Time: 4:58 PM
 */
class Importer {

	/** @var String */
	var $spreadsheet_path;

	/** @var array 需要录入的 sheet 列表 */
	var $listed_sheet_names = array();

	/** @var  Spreadsheet */
	var $spreadsheet;

	var $start_date;

	var $end_date;

	/** @var  mysqli */
	private $connection;

	function load_spreadsheet( $excel_file_path, $start_date, $end_date ) {
		$this->database();

		$this->spreadsheet_path = realpath( $excel_file_path );

		$this->start_date = $start_date;
		$this->end_date   = $end_date;

		$filename = pathinfo( $this->spreadsheet_path, PATHINFO_FILENAME );
		echo $filename . ' is be checking...' . PHP_EOL;
		if ( $this->get_order_sheet_names() ) {

			echo "\t" . 'file is loaded to memory.' . PHP_EOL;
			echo "\t\t" . 'memory usage before loading it: ' . memory_get_usage( true ) . PHP_EOL;
			$this->spreadsheet = IOFactory::load( $excel_file_path );
			echo "\t\t" . 'memory usage after loading it: ' . memory_get_usage( true ) . PHP_EOL;
		} else {
			echo "\t" . 'no listed sheet in this file. Skipped it.' . PHP_EOL;
		}
	}

	function database() {
		if ( ! $this->connection || ! $this->connection->ping() ) {
			$this->connection = new mysqli( DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE );
			if ( $this->connection->connect_error ) {
				die( 'Connect Error (' . $this->connection->connect_errno . ') '
				     . $this->connection->connect_error );
			}
		}

		return $this->connection;
	}


	function data() {
		if ( ! $this->listed_sheet_names || ! $this->spreadsheet ) {
			return;
		}

		foreach ( $this->listed_sheet_names as $sheet_name ) {
			$sheet_data = $this->get_sheet_data( $sheet_name );
			$this->save( $sheet_data );
			$this->log( $sheet_name );
			unset( $sheet_data );
		}
	}

	public function get_order_sheet_names() {
		$path   = 'zip://' . $this->spreadsheet_path . '#xl/workbook.xml';
		$sheets = simplexml_load_string( file_get_contents( $path ) );
		if ( ! $sheets ) {
			echo 'did not found workbook.xml in this file' . PHP_EOL;

			return false;
		}

		foreach ( $sheets->sheets->sheet as $sheet ) {
			$sheet_name = (string) $sheet['name'];
			if ( $this->is_valid_sheet( $sheet_name ) ) {
				// 如果出现没有前导 0 的 sheet name，就需要修改代码了
				$this->listed_sheet_names[] = $sheet_name;
			}
		}

		return $this->listed_sheet_names;
	}

	public function get_sheet_data( $sheet_name ) {
		$sheet = $this->spreadsheet->getSheetByName( $sheet_name );

		$sheet_columns = array();
		$sheet_data    = array();
		$start_row     = 2;

		if ($sheet->getHighestRow() <= $start_row) {
			// no data in this sheet
			return $sheet_data;
		}

		foreach ( $sheet->getRowIterator( $start_row ) as $row ) {
			$row_data = array();

			$column_row = ( $row->getRowIndex() == 2 );
			foreach ( $row->getCellIterator() as $cell ) {
				if ( ! $cell->isInRange( "A1:S10000" ) ) {
					// 暂时不处理 S 列以后的内容
					break;
				}

				$column = $cell->getColumn();
				$value  = $cell->getValue();

				// column data
				if ( $column_row ) {
					// 有重名的 column
					switch ( $column ) {
						case "Q" :
							$value = 'Condition1';
							break;
						case "AK" :
							$value = 'Condition2';
							break;
						case "AE" :
							$value = 'ACCOUNT1';
							break;
						case "AI" :
							$value = 'ACCOUNT2';
							break;
					}

					// 去除 column 名称中的空格，与 database 中的风格保持一致
					$value = str_replace( ' ', '-', $value );

					$sheet_columns[ $column ] = $value;
					continue;
				}

				// order data
				$column_index = $sheet_columns[ $column ];

				// date format
				if ( $column_index == 'purchase-date' ) {
					$temp = NumberFormat::toFormattedString( $value, NumberFormat::FORMAT_DATE_DATETIME );
					$value = date( 'Y-m-d H:i', strtotime( $temp ) );
				}

				// no functional data
				$row_data[ $column_index ] = ( strpos( ltrim( $value ), '=' ) === 0 ? '' : $value );

				if ( $column == 'A' ) {
					// 无效订单
					$row_data['gray_row'] = $this->is_gray_cell( $cell );
				}

				if ( $column == 'B' && ! $this->is_order( $cell->getValue() ) ) {
					// 非订单条目
					unset( $row_data );
					continue 2;
				}
			}
			$sheet_data[] = $row_data;
		}

		return $sheet_data;
	}

	public function is_valid_sheet( $sheet_name ) {
		$formatted = str_replace( array( '/', ' ' ), array( '' ), $sheet_name );
		if ( ! preg_match( '/[0-9]{4}/', $formatted ) ) {
			// not a order sheet
			return false;
		}
		if ( ( $formatted < $this->start_date ) || ( $formatted > $this->end_date ) ) {
			// not in date range
			return false;
		}

		// check import logs
		$filename      = pathinfo( $this->spreadsheet_path, PATHINFO_FILENAME );
		$modified_time = filemtime( $this->spreadsheet_path );

		$sql    = 'SELECT count(*) as count FROM import_logs WHERE `filename` = "' . addslashes( $filename ) . '"'
		          . ' AND sheet_name = "' . addslashes( $sheet_name ) . '"'
		          . ' AND imported_date > ' . $modified_time;
		$result = $this->connection->query( $sql );
		$count  = $result->fetch_array( MYSQLI_NUM );
		if ( $count[0] ) {
			return false;
		}

		return true;
	}

	public function log( $sheet_name ) {
		$filename = pathinfo( $this->spreadsheet_path, PATHINFO_FILENAME );

		$sql = 'INSERT INTO import_logs SET `filename` = "' . addslashes( $filename ) . '"'
		       . ' , sheet_name = "' . addslashes( $sheet_name ) . '"';
		$this->connection->query( $sql );
	}

	public function is_gray_cell( Cell $cell ) {
		$color = $cell->getStyle()->getFill()->getStartColor()->getRGB();

		return ( $color && $this->is_gray( $color ) );
	}

	public function is_gray( $color ) {
		$temp = str_split( $color, 2 );

		return ( sizeof( array_unique( $temp ) ) == 1 && $temp[0] != '00' );
	}

	/**
	 * @link https://pay.amazon.com/us/help/81733
	 *
	 * @param string $order_id
	 *
	 * @return bool
	 */
	public function is_order( $order_id ) {
		return ( $order_id && preg_match( '/^[0-9]{3}-[0-9]{7}-[0-9]{7}$/', $order_id ) );
	}

	public function save( $data ) {
		$this->database();

		$columns = array(
			'gray_row',
			'status',
			'order-id',
			'recipient-name',
			'purchase-date',
			'sku-address',
			'sku',
			'price',
			'quantity-purchased',
			'shipping-fee',
			'ship-state',
			'isbn-address',
			'isbn',
			'SELLER',
			'SELLER-ID',
			'SELLER-PRICE',
			'URL',
			'Condition1',
			'character',
			'Remark',
		);
		// @todo NF3
		foreach ( $data as $row ) {
			if ( empty( $row ) || ! array_key_exists( 'order-id', $row ) ) {
				continue;
			}

			// @todo 更新最近的 order-id 的处理信息
			$sql    = 'SELECT count(*) as count FROM raw_orders WHERE `order-id` = "' . $row['order-id'] . '"';
			$result = $this->connection->query( $sql );
			$count  = $result->fetch_array( MYSQLI_NUM );
			if ( $count[0] ) {
				continue;
			}

			$sql = "INSERT INTO raw_orders SET ";
			foreach ( $row as $column => $value ) {
				if ( ! in_array( $column, $columns ) ) {
					continue;
				}
				$value = substr( $value, 0, 249 );
				$sql .= '`' . $column . '` = "' . addslashes( $value ) . '",';
			}
			if ( ! $this->connection->query( rtrim( $sql, ',' ) ) ) {
				die( "Error: %s\n" . $this->connection->error );
			}
		}
	}

	public function __destruct() {
		unset( $this->spreadsheet );
	}
}