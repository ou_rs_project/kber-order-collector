<?php
/**
 * 将 sync.php 下载的文件导入数据库
 */

use Rollbar\Payload\Level;
use Rollbar\Rollbar;

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/import/config.php';
require_once __DIR__ . '/import/Importer.php';
require_once __DIR__ . '/misc/rollbar.php';

/**
 * 2M 的 Excel 需要 200M 的内存，5M 的 Excel 需要 900M 的内存
 * 可能的优化方式：
 *   1, 从 google drive 导出为 html 格式然后使用 dom/css 来解析
 *   2, 阅读 Office Open XML 的格式写简单的 parser
 *   3, 将 PHPSpreadsheet 做减法，删除不需要的 style
 */
ini_set( 'memory_limit', MAXIMUM_MEMORY );

// get all .xlsx files from local folder
// .xlsx 文件路径是 sync.php 与 import.php 共用的规则
$local_files = glob( 'files/*/*/*.xlsx' );

// 导入数据至数据库中
foreach ( $local_files as $file ) {
	try {
		if ( stripos( $file, "history" ) !== false ) {
			continue;
		}

		$start_date = '0401';
		$end_date   = '0503';
		$importer   = new Importer();
		$importer->load_spreadsheet( $file, $start_date, $end_date );

		// 太多数据了，所以直接由 importer 保存，不作为返回值
		$importer->data();

		unset( $importer );
	} catch (Exception $e) {
		if (isset($importer)) {
			unset($importer);
		}

		// report exception
		Rollbar::log(Level::error(), $e);

		continue;
	}
}
