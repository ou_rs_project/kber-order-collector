<?php
require 'config.php';
require 'client.php';

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;

/**
 * @link https://developers.google.com/drive/v3/reference/files/list
 * @link https://developers.google.com/drive/v3/web/search-parameters
 *
 * @param Google_Client $client
 *
 * @return Google_Service_Drive_DriveFile[]
 */
function get_order_files( Google_Client $client) {
	$service = new Google_Service_Drive( $client );

	// 文件更新时间段
	// 每个账户的 order update 文件几乎每一天都有更新，所以这个 modified_time 有些没有意义啊
	// 接口又不能把 sheet 的更新时间作为参数
	// @todo 保存最后一次更新的时间，作为下一次更新的 start_modified_time，实现增量更新
	$start_modified_time = date( 'c', strtotime( '-2 day' ) );
	$end_modified_time = date( 'c' );

	// 文件搜索条件
	$query     = array(
		"name contains 'order'",
		"name contains 'update'",
		"not name contains 'history'",
		"modifiedTime > '" . $start_modified_time . "'",
		"modifiedTime < '" . $end_modified_time . "'",
		"mimeType = 'application/vnd.google-apps.spreadsheet'",
		"sharedWithMe",
	);
	$optParams = array(
		'q'        => implode( " and ", $query ),
		'orderBy'  => 'modifiedTime',    // 更新时间升序排列，先处理旧的数据，后处理新的数据
		'pageSize' => 1000,             // 最大值 1000，相当于不做分页，一次处理尽量多的数据
		'fields'   => 'files(id, name)',
	);
	$results   = $service->files->listFiles( $optParams );

	return $results->getFiles();
}

/**
 * @link https://developers.google.com/drive/v3/reference/files/export
 * @link https://developers.google.com/drive/v3/web/manage-downloads
 *
 * @param Google_Service_Drive_DriveFile[] $files
 * @param Google_Client                    $client
 *
 * @return array
 */
function sync_order_files( $files, Google_Client $client ) {
	$service       = new Google_Service_Drive( $client );
	$ms_excel_type = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
	$fs            = new Filesystem();

	$local_files = array();
	foreach ( $files as $file ) {
		$name      = $file->getName();
		$trim_name = str_ireplace( array( '-', '_', 'orders', 'order', 'updates', 'update', 'acc', ' ' ), '', $name );
		if ( ! preg_match( '/[0-9]{2,3}/', $trim_name, $match ) ) {
			echo "error: \t filename doesn't have account number: \t " . $name . "\n";
			continue;
		}
		$account_number = $match[0];
		$match          = array();

		if ( ! preg_match( '/[a-zA-Z]{2}/', $trim_name, $match ) ) {
			echo "error: \t filename doesn't have market code: \t " . $name . "\n";
			continue;
		}
		$market_code = strtoupper( $match[0] );
		$folder      = 'files/' . $account_number . '/' . $market_code . '/';
		$path     = $folder . '/' . trim( $file->getName() ) . '.xlsx';

		if ($fs->exists($path)) {
			// file is existed, don't download again.
			continue;
		}
		
		try {
			if ( ! $fs->exists( $folder ) ) {
				$fs->mkdir( $folder );
			}

			/** @var GuzzleHttp\Psr7\Response $response */
			$response = $service->files->export( $file->getId(), $ms_excel_type );
			
			$fs->dumpFile( $path, $response->getBody() );
			$local_files[] = $path;

			echo "file downloaded: " . $path . PHP_EOL;
		} catch ( IOExceptionInterface $e ) {
			echo "An error occurred while creating your directory at " . $e->getPath();
			exit( - 1 );
		} catch ( Google_Exception $e ) {
			echo "An Google Error" . $e->getMessage() . ", code: " . $e->getCode() . ", file: " . $file->getName();;
			exit( - 1 );
		} catch ( Exception $e ) {
			echo "An Error" . $e->getMessage() . ", code: ";
			exit( - 1 );
		}
	}

	return $local_files;
}
