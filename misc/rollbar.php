<?php

use \Rollbar\Rollbar;

$config = array(
	// required
	'access_token' => '1a99ec23b3424342ad0589fe1d027ae5',
	// optional - environment name. any string will do.
	'environment' => 'production'
);
Rollbar::init($config);
