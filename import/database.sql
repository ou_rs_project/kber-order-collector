CREATE TABLE `raw_orders`(
  `id` INT NOT NULL AUTO_INCREMENT,
  `gray_row` VARCHAR(1),
  `status` VARCHAR(255),
  `order-id` VARCHAR(255),
  `recipient-name` VARCHAR(255),
  `purchase-date` VARCHAR(255),
  `sku-address` VARCHAR(255),
  `sku` VARCHAR(255),
  `price` VARCHAR(255),
  `quantity-purchased` VARCHAR(255),
  `shipping-fee` VARCHAR(255),
  `ship-state` VARCHAR(255),
  `isbn-address` VARCHAR(255),
  `isbn` VARCHAR(255),
  `SELLER` VARCHAR(255),
  `SELLER-ID` VARCHAR(255),
  `SELLER-PRICE` VARCHAR(255),
  `URL` VARCHAR(255),
  `Condition1` VARCHAR(255),
  `character` VARCHAR(255),
  `Remark` VARCHAR(255),
  PRIMARY KEY(`id`)
) ENGINE = MyISAM ;

CREATE TABLE `import_logs`(
  `id` INT NOT NULL AUTO_INCREMENT,
  `filename` VARCHAR(255),
  `sheet_name` VARCHAR(255),
  `imported_date` TIMESTAMP,
  PRIMARY KEY(`id`)
) ENGINE = MyISAM ;
