<?php
/**
 * @link https://developers.google.com/drive/v3/web/quickstart/php
 * @link https://developers.google.com/sheets/api/quickstart/php
 */

define('APPLICATION_NAME', 'KBer Order Update');
define('CREDENTIALS_PATH', '~/.credentials/kb-order-update.json');

// 授权文件的路径
define('CLIENT_SECRET_PATH', __DIR__ . '/client_secret.json' );

// If modifying these scopes, delete your previously saved credentials
// at ~/.credentials/drive-php-quickstart.json
// 授权的应用，如果有修改，需要手动删除 CREDENTIALS_PATH 后重新申请授权
define('SCOPES', implode(' ', array(
		Google_Service_Drive::DRIVE_READONLY,
		)
));

// 暂时仅仅允许命令行操作
if (php_sapi_name() != 'cli') {
	throw new Exception('This application must be run on the command line.');
}
