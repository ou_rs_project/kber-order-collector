<?php
/**
 * 下载所有的 order data 文件
 */

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/drive/functions.php';

// Get the API client and construct the service object.
$client = getClient();

// 获得被 google drive 文件列表
$files = get_order_files( $client );

// 下载 google drive 文件为 Excel 本地文件
$local_files = sync_order_files( $files, $client );
